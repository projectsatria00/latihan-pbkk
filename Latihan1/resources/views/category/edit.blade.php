@extends('layouts.app')
@section('title','Halaman Category')
@section('main')
<div class="container">
	<div class="row mt-3 mb-3">
    <form action="{{ url('/category/update') }}" method="post" enctype="multipart/form-data">
      @csrf
      <input type="hidden" name="id" value="{{ $category->id }}">
      <div class="mb-3">
        <label>Nama</label>
        <input type="text" class="form-control @if($errors->first('name')) is-invalid @endif" name="name" value="{{ $category->name }}">
        <span class="error invalid-feedback">{{ $errors->first('name') }}</span>
      </div>
      <div class="mb-3">
        <button class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
</div>
@endsection
