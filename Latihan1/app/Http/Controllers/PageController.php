<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    function home(){
			$nilai = 85;
    	return view('welcome', ['nilai' => $nilai]);
    }

    function profile(){
    	return view('profile');
    }

    function news(){
    	return view('news');
    }

    function product(){
    	return view('product');
    }

    function travel(){
    	return view('travel');
    }

    function food(){
    	return view('food');
    }
}
